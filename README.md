# README #
Данный документ содержит информацию, а так же предположения, которые я использовал по мере исполнения.

Задание выполнялось на Intellij IDEA.  
БД: mysql, jdbcTemplate  
Фрэймворк: Spring 4 (Java Based Configuration)  
Apache Tomcat 8.5.20  
Сборщик проектов maven  
OS на которым выполнял задание: MAC OS 

1.Создать таблицу User, прописанную в скрипте   
2.4 пункт задания не совсем был понятен, а именно с отправкой timestamp на сервер. В задании не говорится что делать с ним. Я просил  
пояснения,но мне не ответили, если пояснят, исправлю согласно требованиям
3.Прописать свое название бд, user, password в классе AppConfig.java в методе getDataSource  
Server API  

При запуске по дефолту открывается home.jsp начальная страница.  

REST сервисы:  
 
1. GET запрос http:localhost:8080/users/get/{id} - Воввращает информацию о пользователе, если он существует в таблице, иначе ошибка.  
   Пример запроса:  
   http:localhost:8080/users/get/{2}  
   Ответ:  
   {  
    "id": 2,  
    "firstName": "Dan",  
    "lastName": "Brown",  
    "gender": "male", 
    "email": "dan@gmail.com",  
    "imageUri": null,  
    "status": null,  
    "status_modified_time":0  
	}  
	в случае ошибки:    
	{"error":"Не существует пользователя с ID 5000"}  
2. POST запрос http:localhost:8080/users/create?firstName=VAL&lastName=VAL&gender=VAL&email=VAL&imageUri=VAL - Создать пользователя     
	используя RequestParams  
	Пример запроса:  
	localhost:8080/users/create?firstName=John&lastName=Snow&gender=male&email=john@gmail.com&imageUri=https://pp.userapi.com/c635102/v635102456/188cd/ApRokGKnyM4.jpg    
	Ответ:  
	{  
     "userID": "24"    
	}  
3. 	POST запрос http:localhost:8080/users/create/other - Создать пользователя используя RequestBody  
	Пример запроса:  
	localhost:8080/users/create  
	{  
	  "firstName": "John",  
      "lastName": "Snow",  
      "gender": "male",  
      "email": "male@gmail.com",  
      "imageUri": null,  
       "status": "offline"  
	}  
	Ответ:  
	{  
     "userID": "24"  
	}  
	
4. POST запрос http:localhost:8080/users/status/{id}?status=VAL - Обновляет статус пользователя с id, и возвращает id пользователя,  
	текущий и предыдущий статус.  
	Пример запроса:  
	http://localhost:8080/users/status/2?status=offline  
	Ответ:  
	{  
     "userId": 21,  
     "currentStatus": "offline",  
     "previousStatus": "online"  
	}  
	если пользователъ не существует:  
	{"error":"Не существует пользователя с ID 2"}  

5. GET запрос http:localhost:8080/users/statistics?status=VAL - Возвращает timestamp запроса, и список пользователей со статусом с запроса,  
	если RequestParam не передался, список пользователей без статуса  
	Пример запроса:  
	http://localhost:8080/users/statistics?status=online   
    http://localhost:8080/users/statistics  
	Ответ:  
	{  
    "users": [  
        {  
            "id": 7,  
            "firstName": "John",  
            "lastName": "Snow",  
            "gender": "male",  
            "email": "snow@gmail.com",  
            "imageUri": null,  
            "status": "offline",  
            "status_modified_time": 1506936956982  
        },  
        {  
            "id": 9,  
            "firstName": "Brandan",  
            "lastName": "Stark",  
            "gender": "male",  
            "email": "stark@gmail.com",  
            "imageUri": null,  
            "status": "offline",  
            "status_modified_time": 1506936956954  
        },  
        {  
            "id": 21,  
            "firstName": "Jamie",  
            "lastName": "Lannister",  
            "gender": "male",  
            "email": "lannister@gmail.com",  
            "imageUri": "https://pp.userapi.com/c635102/v635102456/188cd/ApRokGKnyM4.jpg",  
            "status": "offline",  
            "status_modified_time": 1506936956432
        }  
    ],  
    "timestamp": "10.01.2017 23:44:34"  
  }  
