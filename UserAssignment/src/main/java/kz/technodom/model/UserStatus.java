package kz.technodom.model;


/**
 * Created by Tokhtar Yelemessov on 2017-10-01. Вспомогательный класс, для работы со статусами
 */
public class UserStatus {

    //Ид пользователя
    private Long userId;
    //Текущий статус
    private String currentStatus;
    //Предыдущий статус
    private String previousStatus;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(String previousStatus) {
        this.previousStatus = previousStatus;
    }
}
