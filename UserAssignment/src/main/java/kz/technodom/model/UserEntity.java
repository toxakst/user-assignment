package kz.technodom.model;

/**
 * Created by Tokhtar Yelemessov on 2017-09-30.
 */
public class UserEntity {

    //Ид пользователя, в базе прописан автоинкремент
    private Long id;
    //Имя
    private String firstName;
    //Фамилия
    private String lastName;
    //Пол
    private String gender;
    //Email адрес
    private String email;
    //Картинка
    private String imageUri;
    //Статус
    private String status;
    //Время изменения статуса
    private Long status_modified_time;


    public UserEntity(Long id){
        super();
        this.id = id;
    }

    public UserEntity(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getStatus_modified_time() {
        return status_modified_time;
    }

    public void setStatus_modified_time(Long status_modified_time) {
        this.status_modified_time = status_modified_time;
    }
}
