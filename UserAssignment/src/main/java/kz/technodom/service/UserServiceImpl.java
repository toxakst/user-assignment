package kz.technodom.service;

import kz.technodom.model.UserEntity;
import kz.technodom.model.UserStatus;
import kz.technodom.repository.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Tokhtar Yelemessov on 2017-10-01.
 */
@Service
public class UserServiceImpl implements UserService {

    UserDao userDao;


    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * Создание нового пользователя
     * @param user
     * @return ид пользователя
     */
    public Long addUser(UserEntity user) {
        return userDao.addUser(user);
    }

    /**
     * Находит пользователя по ID
     * @param id
     * @return список из одного элемента - уникального пользователя
     */
    public List<UserEntity> findById(Long id) {
        return userDao.findById(id);
    }

    /**
     * Обновляет статус пользователя
     * @param user
     * @param status
     * @return объект Статус пользователя
     */
    public UserStatus updateUserStatus(UserEntity user, String status, Long timestamp) {
        return userDao.updateUserStatus(user, status, timestamp);
    }

    /**
     * Находит пользователей по статусу
     * @param status
     * @return список пользователей, отфильтрованный по статусу
     */
    public List<UserEntity> findUsersByStatus(String status) {
        return userDao.findUsersByStatus(status);
    }
}
