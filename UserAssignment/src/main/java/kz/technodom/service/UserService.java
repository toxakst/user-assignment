package kz.technodom.service;

import kz.technodom.model.UserEntity;
import kz.technodom.model.UserStatus;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Tokhtar Yelemessov on 2017-10-01.
 */
public interface UserService {
    public Long addUser(UserEntity user);
    public List<UserEntity> findById(Long id);
    public UserStatus  updateUserStatus(UserEntity user, String status, Long timestamp);
    public List<UserEntity> findUsersByStatus(String status);
}
