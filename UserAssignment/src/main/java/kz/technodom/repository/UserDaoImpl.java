package kz.technodom.repository;

import kz.technodom.model.UserEntity;
import kz.technodom.model.UserStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Tokhtar Yelemessov on 2017-10-01.
 */
@Repository
public class UserDaoImpl implements UserDao {

    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) throws ClassNotFoundException {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }


    /**
     * Создание нового пользователя
     * @param user
     * @return ид пользователя
     */
    public Long addUser(UserEntity user) {
        KeyHolder holder = new GeneratedKeyHolder();
        String sql = "INSERT INTO User(first_name, last_name, gender, email, image_uri) values (:first_name, :last_name, :gender, :email, :image_uri)";
        namedParameterJdbcTemplate.update(sql, getSqlParameterByModel(user), holder, new String[]{"id"});
        return (Long) holder.getKey();
    }

    /**
     * Находит пользователя по ID
     * @param id
     * @return список из одного элемента ь уникального пользователя
     */
    public List<UserEntity> findById(Long id) {
        String sql = "SELECT id, first_name, last_name, gender, email, image_uri, status, status_modified_time FROM User where id = " + id;
        return namedParameterJdbcTemplate.query(sql, getSqlParameterByModel(new UserEntity(id)), new UserMapper());
    }

    /**
     * Обновляет статус пользователя
     * @param user
     * @param status
     * @return объект Статус пользователя
     */
    public UserStatus updateUserStatus(UserEntity user, String status, Long timestamp) {
        String updateQuery = "UPDATE User set status = '" +status+"', status_modified_time = "+timestamp+" where id = " + user.getId();
        String previousStatus = user.getStatus();
        namedParameterJdbcTemplate.update(updateQuery, getSqlParameterByModel(user));
        UserStatus userStatus = new UserStatus();
        userStatus.setUserId(user.getId());
        userStatus.setCurrentStatus(status);
        userStatus.setPreviousStatus(previousStatus);
        return userStatus;
    }

    /**
     * Находит пользователей по статусу
     * @param status
     * @return список пользователей, отфильтрованный по статусу
     */
    public List<UserEntity> findUsersByStatus(String status) {
        String query = "";
        if (status != null)
            query = "SELECT * from User where status = '" + status + "'";
        else
            query = "SELECT * from User where status is null";
        List<UserEntity> users = namedParameterJdbcTemplate.query(query, getSqlParameterByModel(null), new UserMapper());
        return users;
    }

    /**
     * Маппер значений полей
     * @param user
     * @return
     */
    private SqlParameterSource getSqlParameterByModel(UserEntity user) {
        MapSqlParameterSource map = new MapSqlParameterSource();
        if (user != null) {
            map.addValue("id", user.getId());
            map.addValue("first_name", user.getFirstName());
            map.addValue("last_name", user.getLastName());
            map.addValue("gender", user.getGender());
            map.addValue("email", user.getEmail());
            map.addValue("image_uri", user.getImageUri());
            map.addValue("status", user.getStatus());
            map.addValue("status_modified_time", user.getStatus_modified_time());
        }
        return map;
    }

    /**
     * Маппер результата
     */
    private static final class UserMapper implements RowMapper<UserEntity> {

        public UserEntity mapRow(ResultSet resultSet, int i) throws SQLException {
            UserEntity user = new UserEntity();
            user.setId(resultSet.getLong("id"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setLastName(resultSet.getString("last_name"));
            user.setGender(resultSet.getString("gender"));
            user.setEmail(resultSet.getString("email"));
            user.setImageUri(resultSet.getString("image_uri"));
            user.setStatus(resultSet.getString("status"));
            user.setStatus_modified_time(resultSet.getLong("status_modified_time"));
            return user;
        }
    }
}
