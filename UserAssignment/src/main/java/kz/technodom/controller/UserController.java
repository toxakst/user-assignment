package kz.technodom.controller;

import kz.technodom.model.UserEntity;
import kz.technodom.model.UserStatus;
import kz.technodom.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Tokhtar Yelemessov on 2017-09-30.
 */
@Controller
public class UserController {

    @Autowired
    UserServiceImpl userService;

    /**
     * This is the landing method, that is called when the application starts. Redirects to home.jsp page
     * @return
     */
    @RequestMapping(value = "/")
    public String getHomePage(){
        return "home";
    }

    /**
     * Creates a new user
     * @param firstName
     * @param lastName
     * @param gender
     * @param email
     * @return ID of the newly created user
     */
    @RequestMapping(value = "/users/create", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, String>> createUser(@RequestParam(value = "firstName", required = false)String firstName,
                                                        @RequestParam(value = "lastName", required = false)String lastName,
                                                        @RequestParam(value = "gender", required = false)String gender,
                                                        @RequestParam(value = "email", required = false)String email,
                                                        @RequestParam(value = "imageUri", required = false)String imageUri){
        HashMap<String, String> map = new HashMap<String, String>();
        UserEntity user = new UserEntity();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setGender(gender);
        user.setEmail(email);
        user.setImageUri(imageUri);
        //save to db
        Long id = userService.addUser(user);
        map.put("userID", id.toString());
        return new ResponseEntity<HashMap<String, String>>(map, HttpStatus.OK);
    }

    /**
     * Creates a new user
     * @param user
     * @return ID of the newly created user
     */
    @RequestMapping(value = "/users/create/other", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, String>> createUserOther(@RequestBody UserEntity user){
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/html; charset=utf-8");
        HashMap<String, String> map = new HashMap<String, String>();
        Long id = userService.addUser(user);
        map.put("userID", id.toString());
        return new ResponseEntity<HashMap<String, String>>(map, responseHeaders, HttpStatus.OK);
    }

    /**
     * Get the user information by ID
     * @param id
     * @return UserEntity object
     */
    @RequestMapping(value = "/users/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> getUserById(@PathVariable(value = "id") Long id){
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/html; charset=utf-8");
        List<UserEntity> users = userService.findById(id);
        if (users != null && !users.isEmpty()) {
            return new ResponseEntity<Object>(users.get(0), HttpStatus.OK);
        } else {
            Map<String, String> map = new HashMap<String, String>();
            map.put("error", "Не существует пользователя с ID " + id);
            return new ResponseEntity<Object>(map, responseHeaders, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * updates the status of the user
     * @param id, status
     * @return ID of the user, previous and current statuses
     */
    @RequestMapping(value = "/users/status/{id}", method = RequestMethod.POST)
    public ResponseEntity<Object> updateStatus(@PathVariable(value = "id") Long id,
                                                            @RequestParam(value = "status", required = false)String status){
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("Content-Type", "text/html; charset=utf-8");
        List<UserEntity> users = userService.findById(id);
        if (users != null && !users.isEmpty()) {
            //Status change time
            UserStatus userStatus = userService.updateUserStatus(users.get(0), status, new Date().getTime());
            if (userStatus != null){
                return new ResponseEntity<Object>(userStatus, HttpStatus.OK);
            }else {
                Map<String, String> map = new HashMap<String, String>();
                map.put("error", "Ошибка");
                return new ResponseEntity<Object>(map, responseHeaders, HttpStatus.NOT_FOUND);
            }
        } else {
            Map<String, String> map = new HashMap<String, String>();
            map.put("error", "Не существует пользователя с ID " + id);
            return new ResponseEntity<Object>(map, responseHeaders, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Gets the user according to received status
     * @param status
     * @return List of users filtered by status and a timestamp of request
     */
    @RequestMapping(value = "/users/statistics", method = RequestMethod.GET)
    public ResponseEntity<Object> getServerStatistics(@RequestParam(value = "status", required = false)String status){
        Map<String, Object> map = new HashMap<String, Object>();
        List<UserEntity> users = userService.findUsersByStatus(status);
        map.put("timestamp", new SimpleDateFormat("MM.dd.yyyy HH:mm:ss").format(new Date()));
        map.put("users", users);
        return new ResponseEntity<Object>(map, HttpStatus.OK);
    }
}
